package com.example.AdvanceJava.repository;


import com.example.AdvanceJava.model.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {
    @Query(value = "select * from student where groupid = 1", nativeQuery = true)
    public List<Student> getGroupNo1();
    List<Student> findAllByGroupid(int groupid);
}